# IPFS-cluster with webui

Dirty repo.
Research based.
Sole purpose is to get an ipfs-cluster up and running while seeing things through the webui


## Cluster's case, a brief word

This is an example docker-compose file to quickly test an IPFS Cluster
with multiple peers on a contained environment.

It runs 3 cluster peers (cluster0, cluster1...) attached to go-ipfs daemons
(ipfs0, ipfs1...) using the CRDT consensus component. Cluster peers
autodiscover themselves using mDNS on the docker internal network.

To interact with the cluster use "ipfs-cluster-ctl" (the cluster0 API port is
exposed to the locahost. You can also "docker exec -ti cluster0 sh" and run
it from the container. "ipfs-cluster-ctl peers ls" should show all 3 peers a few
seconds after start.

For persistance, a "compose" folder is created and used to store configurations
and states. This can be used to edit configurations in subsequent runs. It looks
as follows:

```
compose/
|-- cluster0
|-- cluster1
|-- ...
|-- ipfs0
|-- ipfs1
|-- ...
```
During the first start, default configurations are created for all peers.

Run first `ipfs-swarm-key-gen.sh` in order to get a valid swarm key


## Bibligraphy

https://hub.docker.com/r/ipfs/go-ipfs
https://hub.docker.com/r/ipfs/go-ipfs/
https://golang.org/doc/install
https://github.com/ipfs/go-ipfs/issues/5767
https://github.com/Kubuxu/go-ipfs-swarm-key-gen
https://gist.github.com/usmansaleem/bb47064f406c105fdfa69716544b7b8e
https://hub.docker.com/r/consensysllc/go-ipfs
https://github.com/ipfs/go-ipfs/issues/4174
https://github.com/ipfs-shipyard/ipfs-webui
https://docs.ipfs.io/concepts/case-study-morpheus/#the-story
https://github.com/ipfs/go-ipfs/blob/master/Dockerfile
https://serverfault.com/questions/205498/how-to-get-pid-of-just-started-process
https://github.com/ipfs/go-ipfs/issues/4187
https://github.com/ipfs/website/issues/85
https://github.com/ipfs/go-ipfs/blob/master/core/corehttp/webui.go#L4
https://www.reddit.com/r/ipfs/comments/iyw2k7/update_ipfs_webui_version/
https://ipfs-search.com/#/search?kind=directory&search=webui
https://willschenk.com/articles/2019/setting_up_an_ipfs_node/
https://docs.ipfs.io/how-to/pin-files/#three-kinds-of-pins

https://medium.com/@s_van_laar/deploy-a-private-ipfs-network-on-ubuntu-in-5-steps-5aad95f7261b
https://github.com/ipfs/ipfs-cluster/issues/538
https://www.google.com/search?client=firefox-b-e&q=ipfs-cluster+use+case
https://labs.eleks.com/2019/03/ipfs-network-data-replication.html
i

