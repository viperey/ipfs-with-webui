#!/bin/bash
set -eu

function main() {
  echo -e "/key/swarm/psk/1.0.0/\n/base16/\n$(hexdump -n 32 -e '16/1 "%02x"' /dev/urandom)"
}

main "$@"